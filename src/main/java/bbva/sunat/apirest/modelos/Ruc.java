package bbva.sunat.apirest.modelos;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class Ruc {
    @Id
    public String ruc;
    public String tipoContribuyente;
    public String nombreComercial;
    public Date fechaInscripcion;
    public Date fechaInicioActividades;
    public String estadoContribuyente;
    public String condicionContribuyente;
    public String domicilioFiscal;
    public String actividadComercioExterior;
    public String actividadEconomica;
    public String correo;
    public String telefono;
    public Date created_at;
    public Date updated_at;



}
