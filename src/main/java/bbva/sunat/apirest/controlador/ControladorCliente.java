package bbva.sunat.apirest.controlador;

import bbva.sunat.apirest.modelos.Cliente;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CLIENTES)
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    // GET http://localhost:9000/api/v1/clientes?pagina=7&cantidad=3 -> List<Cliente> obtenerClientes()
    @GetMapping
    public PagedModel<EntityModel<Cliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try {
            final var pageSinMetadatos = this.servicioCliente.obtenerClientes(pagina, cantidad);
            final var page = pageSinMetadatos.map(
                    x -> EntityModel.of(x).add(
                            linkTo(methodOn(this.getClass()).obtenerUnCliente(x.documento))
                                    .withSelfRel().withTitle("Este cliente"),
                            linkTo(methodOn(ControladorCuentasCliente.class).obtenerCuentasCliente(x.documento))
                                    .withRel("cuentas").withTitle("Cuentas del cliente")
                    ));

            return this.pagedResourcesAssembler.toModel(page);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {

        try {
        this.servicioCliente.insertarClienteNuevo(cliente);
        final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCliente.class)
                .obtenerUnCliente(cliente.documento);

        final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();

        return ResponseEntity
                .accepted()
                .location(enlaceEsteDocumento)
                .body("Cliente creado con éxito!");
        } catch(Exception x) {
            return new ResponseEntity<>("El documento "+cliente.documento +" ya fue registrado", HttpStatus.NOT_FOUND);
        }

    }

    // GET http://localhost:9000/api/v1/clientes/{documento} -> obtenerUnCliente(documento)
    // GET http://localhost:9000/api/v1/clientes/12345678 -> obtenerUnCliente("12345678")
    @GetMapping("/{documento}")
    public Cliente obtenerUnCliente(@PathVariable String documento) {
        try {
            System.err.println(String.format("obtenerUnCliente %s", documento));
            return this.servicioCliente.obtenerCliente(documento);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> reemplazarUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> reemplazarUnCliente("12345678", DATOS)
    @PutMapping("/{documento}")
    public ResponseEntity reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.guardarCliente(cliente);
            return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }

    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{documento}")
    public ResponseEntity emparacharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        try {
            cliente.documento = nroDocumento;
            this.servicioCliente.emparcharCliente(cliente);
            return new ResponseEntity<>("Cliente actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cliente no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    // DELETE http://localhost:9000/api/v1/clientes/{documento} -> eliminarUnCliente(documento)
    // DELETE http://localhost:9000/api/v1/clientes/12345678 + DATOS -> eliminarUnCliente("12345678")
    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity eliminarUnCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception x) {
            return new ResponseEntity<>("Cliente no encontrado." +documento, HttpStatus.NOT_FOUND);
        }
    }
}
