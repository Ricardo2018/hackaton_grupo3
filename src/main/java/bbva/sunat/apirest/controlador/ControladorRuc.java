package bbva.sunat.apirest.controlador;

import bbva.sunat.apirest.modelos.Ruc;
import bbva.sunat.apirest.servicios.ServicioRuc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Rutas.RUC)
public class ControladorRuc {


    @Autowired
    ServicioRuc servicioRuc;

    @GetMapping("/{ruc}")
    public Ruc obtenerRuc( @PathVariable String ruc) {
        System.out.println("obtenerRuc: "+ ruc);
        return  servicioRuc.procesarRuc(ruc);

    }

}
