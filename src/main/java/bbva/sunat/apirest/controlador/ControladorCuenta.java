package bbva.sunat.apirest.controlador;
import bbva.sunat.apirest.modelos.Cuenta;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // http://localhost:9000/api/v1/cuentas
    @GetMapping
    public List<Cuenta> obtenerCuentas(){
        return this.servicioCuenta.obtenerCuentas();
    }

    // http://localhost:9000/api/v1/cuentas + DATOS
    @PostMapping
    public ResponseEntity agregarCuenta(@RequestBody Cuenta cuenta){

        try {
        this.servicioCuenta.insertarCuentaNueva(cuenta);
        final var representacionMetodoObtenerUnClienteConDocumento = methodOn(ControladorCuenta.class)
                .obtenerunaCuenta(cuenta.numero);

        final var enlaceEsteDocumento = linkTo(representacionMetodoObtenerUnClienteConDocumento).toUri();


        return ResponseEntity
                .accepted()
                .location(enlaceEsteDocumento)
                .body("Cuenta creado con éxito!");
        } catch(Exception x) {
            return new ResponseEntity<>("El ctanumero "+cuenta.numero +" ya fue registrado", HttpStatus.NOT_FOUND);
        }

    }

    // http://localhost:9000/api/v1/cuentas/{documento}
    // http://localhost:9000/api/v1/cuentas/12345678
    @GetMapping("/{numero}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        try {
            return this.servicioCuenta.obtenerCuenta(numero);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // http://localhost:9000/api/v1/cuentas/{documento} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PutMapping("/{numero}")
    public ResponseEntity reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.guardarCuenta(cuenta);

            return new ResponseEntity<>("Cuenta actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cuenta no encontrado.", HttpStatus.NOT_FOUND);
        }
    }

    // http://localhost:9000/api/v1/cuentas/{documento} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PatchMapping("/{numero}")
    public ResponseEntity emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                   @RequestBody Cuenta cuenta) {
        try {
            cuenta.numero = ctanumero;
            this.servicioCuenta.emparcharCuenta(cuenta);
            return new ResponseEntity<>("Cuenta actualizado correctamente.", HttpStatus.OK);
        }catch (Exception x) {
            return new ResponseEntity<>("Cuenta no encontrado." +ctanumero, HttpStatus.NOT_FOUND);
        }
    }

    // DELETE http://localhost:9000/api/v1/cuentas/{numero}
    // DELETE http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity borrarunaCuenta(@PathVariable("numero") String ctanumero) {
        try{
            this.servicioCuenta.borrarCuenta(ctanumero);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch(Exception x) {
            return new ResponseEntity<>("Cuenta no encontrado."+ctanumero, HttpStatus.NOT_FOUND);
        }
    }
}

