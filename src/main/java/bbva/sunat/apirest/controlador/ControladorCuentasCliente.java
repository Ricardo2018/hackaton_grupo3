package bbva.sunat.apirest.controlador;
import bbva.sunat.apirest.modelos.Cuenta;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioCliente;
import bbva.sunat.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    // CRUD - GET *,GET,POST,PUT,PATCH

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioCuenta servicioCuenta;

    public static class DatosEntradaCuenta {
        public String codigoCuenta;
    };

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS -> agregarCuentaCliente(documento,cuenta)
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody DatosEntradaCuenta datosEntradaCuenta) {
        try {

            this.servicioCliente.agregarCuentaCliente(documento, datosEntradaCuenta.codigoCuenta);
        } catch(ObjetoNoEncontrado x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<List<String>> obtenerCuentasCliente(@PathVariable String documento) {
        try {
            final var cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch(ObjetoNoEncontrado x) {
            return ResponseEntity.notFound().build();
        }
    }


    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> obtenerCuentaCliente(documento,numeroCuenta)
    @GetMapping("/{numeroCuenta}")
    public ResponseEntity<Cuenta> obtenerCuentaCliente(@PathVariable String documento, @PathVariable String numeroCuenta) {
        try {
            final var cuenta = this.servicioCuenta.obtenerCuentaCliente(documento, numeroCuenta);
            return ResponseEntity.ok(cuenta);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // DELETE http://localhost:9000/api/v1/clientes/12345678/cuentas/093840394 -> eliminarCuentaCliente(documento,numeroCuenta)
    @DeleteMapping("/{numeroCuenta}")
    public ResponseEntity eliminarCuentaCliente(@PathVariable String documento, @PathVariable String numeroCuenta) {
        try {
            this.servicioCuenta.eliminarCuentaCliente(documento, numeroCuenta);
        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.noContent().build();
    }
}

