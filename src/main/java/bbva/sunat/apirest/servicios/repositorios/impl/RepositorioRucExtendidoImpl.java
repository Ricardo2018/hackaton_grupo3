package bbva.sunat.apirest.servicios.repositorios.impl;

import bbva.sunat.apirest.modelos.Ruc;
import bbva.sunat.apirest.servicios.repositorios.RepositorioClienteExtendido;
import bbva.sunat.apirest.servicios.repositorios.RepositorioRucExtendido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class RepositorioRucExtendidoImpl implements RepositorioRucExtendido {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharRuc(Ruc parche) {
        final Query query = query(where("_id").is(parche.ruc));
        final Update update = new Update();

        set(update, "tipoContribuyente", parche.tipoContribuyente);
        set(update, "nombreComercial", parche.nombreComercial);
        set(update, "fechaInscripcion", parche.fechaInscripcion);
        set(update, "fechaInicioActividades", parche.fechaInicioActividades);
        set(update, "estadoContribuyente", parche.estadoContribuyente);
        set(update, "condicionContribuyente", parche.condicionContribuyente);
        set(update, "domicilioFiscal", parche.domicilioFiscal);
        set(update, "actividadComercioExterior", parche.actividadComercioExterior);
        set(update, "actividadEconomica", parche.actividadEconomica);
       // set(update, "created_at", parche.created_at);
        set(update, "updated_at", parche.updated_at);

        mongoOperations.updateFirst(query, update, "ruc");
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }
}
