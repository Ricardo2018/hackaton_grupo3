package bbva.sunat.apirest.servicios.repositorios;

import bbva.sunat.apirest.modelos.Cuenta;

public interface  RepositorioCuentaExtendido {

    public void emparcharCuenta(Cuenta cuenta);
}
