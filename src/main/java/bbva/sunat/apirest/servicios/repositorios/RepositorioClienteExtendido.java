package bbva.sunat.apirest.servicios.repositorios;

import bbva.sunat.apirest.modelos.Cliente;

public interface RepositorioClienteExtendido {

    public void emparcharCliente(Cliente parche);

}
