package bbva.sunat.apirest.servicios.repositorios.impl;


import bbva.sunat.apirest.modelos.Cuenta;
import bbva.sunat.apirest.servicios.repositorios.RepositorioCuentaExtendido;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import static org.springframework.data.mongodb.core.query.Query.*;
import static org.springframework.data.mongodb.core.query.Criteria.*;

public class RepositorioCuentaExtendidoImpl implements RepositorioCuentaExtendido {

    @Autowired
    MongoOperations mongoOperations;

    @Override
    public void emparcharCuenta(Cuenta cuenta) {
        final Query query = query(where("_id").is(cuenta.numero));
        final Update update = new Update();

        set(update, "moneda", cuenta.moneda);
        set(update, "estado", cuenta.estado);
        set(update, "oficina", cuenta.oficina);
        set(update, "saldo", cuenta.saldo);
        set(update, "tipo", cuenta.tipo);


        mongoOperations.updateFirst(query, update, "cuenta");
    }

    private void set(Update update, String nombre, Object valor) {
        if(valor != null) {
            System.err.println(String.format("==== MODIFICAR CAMPO %s = %s", nombre, valor));
            update.set(nombre, valor);
        }
    }
}
