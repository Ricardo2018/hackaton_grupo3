package bbva.sunat.apirest.servicios.repositorios;


import bbva.sunat.apirest.modelos.Ruc;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepositorioRuc extends MongoRepository<Ruc, String>,
        RepositorioRucExtendido {

    public Optional<Ruc> findByRuc(String ruc);

    @Query(value = "{nombre: ?0, edad: ?1}")
    public List<Ruc> obtenerRucsPorNombreEdad(String nombre, String edad);

    public void deleteByRuc(String ruc);
}

