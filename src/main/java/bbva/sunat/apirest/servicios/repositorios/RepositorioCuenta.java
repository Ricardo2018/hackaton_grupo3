package bbva.sunat.apirest.servicios.repositorios;



import bbva.sunat.apirest.modelos.Cuenta;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String>,RepositorioCuentaExtendido {


    public void deleteById(String numeroCuenta);

}