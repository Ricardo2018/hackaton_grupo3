package bbva.sunat.apirest.servicios.impl;

import bbva.sunat.apirest.modelos.Ruc;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioRuc;
import bbva.sunat.apirest.servicios.repositorios.RepositorioCuenta;
import bbva.sunat.apirest.servicios.repositorios.RepositorioRuc;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

@Service
public class ServicioRucImpl implements ServicioRuc {

    @Autowired
    RepositorioRuc repositorioRuc;

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Value("${api.externa.v1}")
    private String urlExterna;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public Page<Ruc> obtenerRucs(int pagina, int cantidad) {
        return this.repositorioRuc.findAll(PageRequest.of(pagina, cantidad));
    }

    @Override
    public void insertarRucNuevo(Ruc Ruc) {
        this.repositorioRuc.insert(Ruc);
    }

    @Override
    public Ruc obtenerRuc(String ruc) {
        final Optional<Ruc> quizasRuc = this.repositorioRuc.findByRuc(ruc);
        System.out.println("ingreos == ");
        //if (!quizasRuc.isPresent())
          //  throw new ObjetoNoEncontrado("No existe el Ruc : " + ruc);
        return quizasRuc.get();
    }

    @Override
    public Ruc procesarRuc(String ruc) {
        if (!this.repositorioRuc.existsById(ruc)) {
              System.out.println(" no existe ruc  ");
              Ruc rucCliente= ObtenerInformacionExternoRuc(ruc);
              System.out.println(" rucCliente  "+rucCliente.ruc);
              this.repositorioRuc.save(rucCliente);
            System.out.println("grabar ruc ");
              return rucCliente;

        }else {
            System.out.println("  existe ruc  ");
            Ruc obtenerRucCliente=  obtenerRuc(ruc);
            try {


                Date updated_at =obtenerRucCliente.updated_at;
                System.out.println(" Ultima Actualizacion:: "+ updated_at);
                Date fechaActual = new Date(System.currentTimeMillis());
                System.out.println(" fechaActual: "+ fechaActual);

                int milisecondsByDay = 86400000;
                int dias = (int) ((fechaActual.getTime()-updated_at.getTime()) / milisecondsByDay);
                System.out.println(" dias: "+ dias);
                if(dias<15){
                    System.out.println(" Los dias son mayor a 15 dias");
                    return obtenerRucCliente;
                }else{

                    obtenerRucCliente= ObtenerInformacionExternoRuc(ruc);
                    Date fechaactual = new Date(System.currentTimeMillis());
                    obtenerRucCliente.updated_at=fechaactual;
                    emparcharRuc(obtenerRucCliente);
                    return obtenerRucCliente;
                }

            } catch(Exception x) {
                System.out.println(x.getMessage());
                throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
            }
        }


    }

    @Override
    public Ruc ObtenerInformacionExternoRuc(String ruc) {

        final var respuesta = restTemplate.getForEntity(urlExterna+"/"+ruc, String.class);
        if(respuesta.getStatusCode().isError())
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);

        final var strJson = respuesta.getBody();
        System.out.println(strJson);
        final ObjectMapper mapper = new ObjectMapper();
        final var rucCliente = new Ruc();
        try {
            final var raiz = mapper.readTree(strJson);

            rucCliente.ruc = raiz.get("empresa").get("ruc").asText();
            rucCliente.tipoContribuyente = raiz.get("empresa").get("tipoContribuyente").asText();
            rucCliente.nombreComercial = raiz.get("empresa").get("nombreComercial").asText();

            rucCliente.estadoContribuyente = raiz.get("empresa").get("estadoContribuyente").asText();
            rucCliente.condicionContribuyente = raiz.get("empresa").get("condicionContribuyente").asText();
            rucCliente.domicilioFiscal = raiz.get("empresa").get("domicilioFiscal").asText();
            rucCliente.actividadComercioExterior = raiz.get("empresa").get("actividadComercioExterior").asText();
            rucCliente.actividadEconomica = raiz.get("empresa").get("actividadEconomica").asText();
            rucCliente.correo = raiz.get("empresa").get("correo").asText();
            rucCliente.telefono = raiz.get("empresa").get("telefono").asText();

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date fechaInscripcion = formato.parse(raiz.get("empresa").get("fechaInscripcion").asText().toString().replace("T"," "));
            Date fechaInicioActividades = formato.parse(raiz.get("empresa").get("fechaInicioActividades").asText().toString().replace("T"," "));

            String fechaRegistroActual = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

            Date created_at=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaRegistroActual);
            Date updated_at=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fechaRegistroActual);

            rucCliente.fechaInscripcion = fechaInscripcion;
            rucCliente.fechaInicioActividades = fechaInicioActividades;
            rucCliente.created_at = created_at;
            rucCliente.updated_at = updated_at;

        } catch(Exception x) {
            System.out.println(x.getMessage()+ x.getStackTrace());
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY);
        }

        return rucCliente;

    }


    @Override
    public void emparcharRuc(Ruc parche) {

        if (!this.repositorioRuc.existsById(parche.ruc))
            throw new ObjetoNoEncontrado("No existe el Ruc con documento " + parche.ruc);
        this.repositorioRuc.emparcharRuc(parche);
    }


    @Override
    public void borrarRuc(String ruc) {
        if (!this.repositorioRuc.existsById(ruc))
            throw new ObjetoNoEncontrado("No existe el Ruc : " + ruc);
        this.repositorioRuc.deleteByRuc(ruc);
    }

    @Override
    public void agregarCuentaRuc(String documento, String numeroCuenta) {
        final Ruc Ruc = obtenerRuc(documento);
        System.out.println("==== numeroCuenta ===="+numeroCuenta);
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);

        //Ruc.codigoCuentas.add(numeroCuenta);
        this.repositorioRuc.save(Ruc);
    }

    @Override
    public List<String> obtenerCuentasRuc(String documento) {
        final Ruc Ruc = obtenerRuc(documento);
        return Collections.singletonList(Ruc.ruc);
    }
}
