package bbva.sunat.apirest.servicios.impl;
import bbva.sunat.apirest.servicios.repositorios.RepositorioCuenta;
import bbva.sunat.apirest.modelos.Cuenta;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioCliente;
import bbva.sunat.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Autowired
    ServicioCliente servicioCliente;

    @Override
    public List<Cuenta> obtenerCuentas() {
        return this.repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.repositorioCuenta.insert(cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        final var quizasCuenta = this.repositorioCuenta.findById(numero);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numero);
        return quizasCuenta.get();
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {

        if (!this.repositorioCuenta.existsById(cuenta.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + cuenta.numero);
        this.repositorioCuenta.emparcharCuenta(cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {

        if (!this.repositorioCuenta.existsById(parche.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + parche.numero);
        this.repositorioCuenta.emparcharCuenta(parche);
    }

    @Override
    public void borrarCuenta(String numeroCuenta) {

        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);

        this.repositorioCuenta.deleteById(numeroCuenta);

    }

    @Override
    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta) {
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);

        final var cliente = this.servicioCliente.obtenerCliente(documento);

        if(!cliente.codigoCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada: " + numeroCuenta);

        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if(!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);

        return quizasCuenta.get();
    }

    @Override
    public void eliminarCuentaCliente(String documento, String numeroCuenta) {
        final var cliente = this.servicioCliente.obtenerCliente(documento);
        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if(!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta " + numeroCuenta);
        if(!cliente.codigoCuentas.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada: " + numeroCuenta);
        final var cuenta = quizasCuenta.get();
        cuenta.estado = "INACTIVA";
        cliente.codigoCuentas.remove(numeroCuenta);
        guardarCuenta(cuenta);
        this.servicioCliente.guardarCliente(cliente);
    }
}

