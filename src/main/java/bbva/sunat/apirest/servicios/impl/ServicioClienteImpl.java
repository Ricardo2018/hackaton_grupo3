package bbva.sunat.apirest.servicios.impl;

import bbva.sunat.apirest.servicios.repositorios.RepositorioCliente;
import bbva.sunat.apirest.servicios.repositorios.RepositorioCuenta;
import bbva.sunat.apirest.modelos.Cliente;
import bbva.sunat.apirest.servicios.ObjetoNoEncontrado;
import bbva.sunat.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Override
    public Page<Cliente> obtenerClientes(int pagina, int cantidad) {
        return this.repositorioCliente.findAll(PageRequest.of(pagina, cantidad));
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {
        this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente obtenerCliente(String documento) {
        final Optional<Cliente> quizasCliente = this.repositorioCliente.findById(documento);
        if (!quizasCliente.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + documento);
        return quizasCliente.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if (!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + cliente.documento);
        this.repositorioCliente.emparcharCliente(cliente);
    }

    @Override
    public void emparcharCliente(Cliente parche) {

        if (!this.repositorioCliente.existsById(parche.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + parche.documento);
        this.repositorioCliente.emparcharCliente(parche);
    }

    @Override
    public void borrarCliente(String documento) {
        if (!this.repositorioCliente.existsById(documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + documento);
        this.repositorioCliente.deleteByDocumento(documento);
    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = obtenerCliente(documento);
        System.out.println("==== numeroCuenta ===="+numeroCuenta);
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número " + numeroCuenta);

        cliente.codigoCuentas.add(numeroCuenta);
        this.repositorioCliente.save(cliente);
    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return cliente.codigoCuentas;
    }
}
