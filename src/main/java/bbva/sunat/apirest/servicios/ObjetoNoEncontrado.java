package bbva.sunat.apirest.servicios;

public class ObjetoNoEncontrado extends RuntimeException {
    public ObjetoNoEncontrado(String message) {
        super(message);
    }
}
