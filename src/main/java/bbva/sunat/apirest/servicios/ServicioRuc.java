package bbva.sunat.apirest.servicios;
import bbva.sunat.apirest.modelos.Ruc;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioRuc {

    // CRUD

    public Page<Ruc> obtenerRucs(int pagina, int cantidad);

    // CREATE
    public void insertarRucNuevo(Ruc Ruc);

    // READ
    public Ruc obtenerRuc(String documento);

    // UPDATE (solamente modificar, no crear).
    public Ruc procesarRuc(String Ruc);
    public void emparcharRuc(Ruc parche);
    public Ruc ObtenerInformacionExternoRuc(String Ruc) ;

    // DELETE
    public void borrarRuc(String documento);


    public void agregarCuentaRuc(String documento, String numeroCuenta);

    public List<String> obtenerCuentasRuc(String documento);
}

